package org.piveraproject.data_transfer;

import static org.piveraproject.PiVeraProjectConstants.APICallType.GET_ALL;
import static org.piveraproject.PiVeraProjectConstants.APICallType.GET_ONE;
import static org.piveraproject.PiVeraProjectConstants.APICallType.POST;
import static org.piveraproject.PiVeraProjectConstants.GET_ALL_RESOURCE;
import static org.piveraproject.PiVeraProjectConstants.GET_ONE_RESOURCE;
import static org.piveraproject.PiVeraProjectConstants.POST_ONE_RESOURCE;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.lang3.StringUtils;
import org.piveraproject.Main;
import org.piveraproject.PiVeraProjectConstants;
import org.piveraproject.serveritemtypes.GenericItem;
import org.piveraproject.utilities.Utilities;

import com.google.gson.Gson;

import android.content.Context;
import android.os.AsyncTask;

public class PiVeraAPI {

  private HttpsURLConnection buildConnection(
      String url,
      String userID,
      String password,
      PiVeraProjectConstants.APICallType type,
      String name,
      String outboundTriggerOrValue)
      throws IOException {

    if (type == GET_ONE) {
      url = url + "?name=" + name;
    }

    HttpsURLConnection urlConnection = (HttpsURLConnection) new URL(url).openConnection();
    urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
    urlConnection.setRequestProperty("userID", userID);
    urlConnection.setRequestProperty("password", password);
    urlConnection.setReadTimeout(15000);
    urlConnection.setConnectTimeout(15000);
    urlConnection.setDoInput(true);
    urlConnection.setInstanceFollowRedirects(false);
    urlConnection.setRequestMethod(type.requestMethodName);

    Map<String, Object> params = new LinkedHashMap<>();
    if (type == POST) {
      params.put("name", name);
      params.put("input", outboundTriggerOrValue);
      StringBuilder postData = new StringBuilder();
      for (Map.Entry<String, Object> param : params.entrySet()) {
        if (postData.length() != 0) postData.append('&');
        postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
        postData.append('=');
        postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
      }
      urlConnection.getOutputStream().write(postData.toString().getBytes("UTF-8"));
      urlConnection.getOutputStream().flush();
      urlConnection.getOutputStream().close();
    }

    return urlConnection;
  }

  private String useConnection(Context context, HttpsURLConnection urlConnection) {
    String result = "";
    try {
      urlConnection.connect();
      int status = urlConnection.getResponseCode();
      if (status == 200) {
        BufferedReader br =
            new BufferedReader(new InputStreamReader((urlConnection.getInputStream())));
        StringBuilder sb = new StringBuilder();
        String output;
        while ((output = br.readLine()) != null) {
          sb.append(output);
        }
        result = sb.toString();
      } else if (status == 400) {
        Utilities.postNotification(context, "Response: [400] Malformed Request.");
        return "";
      } else if (status == 401) {
        Utilities.postNotification(context, "Response: [401] Permission Denied.");
        return "";
      } else {
        Utilities.postNotification(context, "Response Error Code: " + status);
        return "";
      }
    } catch (Exception e) {
      Utilities.postNotification(context, "Internal App Error Caught: " + e.getMessage());
      return "";
    }
    if (context instanceof Main) {
      Utilities.clearAllSpinners(context);
    }
    return result;
  }

  public void performGetAll(Context context, String address, String userID) {
    new APIGetAll(context)
        .execute(address + GET_ALL_RESOURCE, userID, Utilities.getUserPasscode(context), "", "");
  }

  public void performGetOne(Context context, String address, String userID, String deviceName) {
    new APIGetOne(context)
        .execute(
            address + GET_ONE_RESOURCE, userID, Utilities.getUserPasscode(context), deviceName, "");
  }

  public void performPost(
      Context context,
      String address,
      String userID,
      String command,
      String outboundTriggerOrValue) {
    new APIPost(context)
        .execute(
            address + POST_ONE_RESOURCE,
            userID,
            Utilities.getUserPasscode(context),
            command,
            outboundTriggerOrValue);
  }

  private class APIGetAll extends AsyncTask<String, String, String> {

    Context context;

    public APIGetAll(Context context) {
      this.context = context;
    }

    @Override
    protected String doInBackground(String... params) {
      // the GET command should ALWAYS be secure, so someone can't use Postman and see what the
      // state of front door lock is.
      try {
        // params here are: String url, String userID, String password, String command, String
        // outboundTriggerOrValue
        return useConnection(
            context,
            buildConnection(params[0], params[1], params[2], GET_ALL, params[3], params[4]));
      } catch (IOException e) {
        Utilities.postNotification(context, "Error Caught: " + e.getMessage());
        Utilities.clearAllSpinners(context);
      }
      return "";
    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      if (result != null && !result.trim().isEmpty() && context instanceof Main) {
        GenericItem[] genericItems = null;
        try {
          genericItems = new Gson().fromJson(result, GenericItem[].class);

          Arrays.sort(
              genericItems,
              new Comparator<GenericItem>() {
                @Override
                public int compare(GenericItem genericItem1, GenericItem genericItem2) {
                  return genericItem1
                      .getName()
                      .getHumanReadable()
                      .compareTo(genericItem2.getName().getHumanReadable());
                }
              });
        } catch (Exception e) {
          // ignore it
        }
        ((Main) context).getInstance().updateAllData(genericItems);
        ((Main) context).getInstance().refreshEveryRowsValue();
      }

      //                //This condition needs to be here. Launching a 'shortcut' will break the app
      // without it.
      //                if(context instanceof Main){
      //                    ((Main) context).getInstance().updateAllData(new ArrayList());
      //                }

      Utilities.clearAllSpinners(context);
    }
  }

  private class APIGetOne extends AsyncTask<String, String, String> {

    Context context;

    public APIGetOne(Context context) {
      this.context = context;
    }

    @Override
    protected String doInBackground(String... params) {
      // the GET command should ALWAYS be secure, so someone can't use Postman and see what the
      // state of front door lock is.
      try {
        // params here are: String url, String userID, String password, String command, String
        // outboundTriggerOrValue
        return useConnection(
            context,
            buildConnection(params[0], params[1], params[2], GET_ONE, params[3], params[4]));
      } catch (IOException e) {
        Utilities.postNotification(context, "Error Caught: " + e.getMessage());
        Utilities.clearAllSpinners(context);
      }
      return "";
    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);

      if (StringUtils.isNotEmpty(result) && context instanceof Main) {
        GenericItem genericItem = new Gson().fromJson(result, GenericItem.class);

        if (genericItem != null && context instanceof Main) {
          ((Main) context).getInstance().updateSingleLine(genericItem);
        }
      } else {
        Utilities.clearAllSpinners(context);
      }
    }
  }

  private class APIPost extends AsyncTask<String, String, String> {

    Context context;

    public APIPost(Context context) {
      this.context = context;
    }

    @Override
    protected String doInBackground(String... params) {
      // the GET command should ALWAYS be secure, so someone can't use Postman and see what the
      // state of front door lock is.
      try {
        return useConnection(
            context, buildConnection(params[0], params[1], params[2], POST, params[3], params[4]));
      } catch (IOException e) {
        Utilities.postNotification(context, "Error Caught: " + e.getMessage());
      }
      return "";
    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);

      if (StringUtils.isNotEmpty(result) && context instanceof Main) {
        GenericItem genericItem = new Gson().fromJson(result, GenericItem.class);

        if (genericItem != null && context instanceof Main) {
          ((Main) context).getInstance().updateSingleLine(genericItem);
        }
      } else {
        Utilities.clearAllSpinners(context);
      }

      //                //This condition needs to be here. Launching a 'shortcut' will break the app
      // without it.
      //                if(context instanceof Main){
      //                    ((Main) context).getInstance().updateAllData(new ArrayList());
      //                }

      if (context instanceof Main) {
        ((Main) context).getInstance().stopRefreshingAnimation();
      }
    }
  }
}
