package org.piveraproject.data_transfer;

import org.piveraproject.PiVeraProjectConstants;
import org.piveraproject.auth.BiometricCallback;
import org.piveraproject.utilities.Utilities;

import android.content.Context;

public class SendDelegate implements BiometricCallback {
  private Context context;
  private String userID;
  private String outboundTriggerOrValue;
  private PiVeraProjectConstants.APICallType callType;

  public SendDelegate(Context context) {
    this.context = context;
  }

  public SendDelegate(
      Context context,
      String userID,
      String outboundTriggerOrValue,
      PiVeraProjectConstants.APICallType callType) {
    this.context = context;
    this.userID = userID;
    this.outboundTriggerOrValue = outboundTriggerOrValue;
    this.callType = callType;
  }

  public void clearMembers() {
    context = null;
    userID = "";
    outboundTriggerOrValue = "";
  }

  @Override
  public void onBiometricAuthenticationNotSupported() {
    Utilities.showToast(context, "Device does not support biometric authentication");
    Utilities.clearAllSpinners(context);
    clearMembers();
  }

  @Override
  public void onBiometricAuthenticationNotAvailable() {
    Utilities.showToast(context, "Fingerprint is not registered in device");
    Utilities.clearAllSpinners(context);
    clearMembers();
  }

  @Override
  public void onBiometricAuthenticationPermissionNotGranted() {
    Utilities.showToast(context, "Permission is not granted by user");
    Utilities.clearAllSpinners(context);
    clearMembers();
  }

  @Override
  public void onAuthenticationFailed() {
    Utilities.showToast(context, "Login not successful");

    //        DO NOT CLEAR DATA here. This fires if the user tries
    //        the wrong finger, which would create a NPE if they
    //        then try the correct finger.
    // clearMembers();
  }

  @Override
  public void onAuthenticationCancelled() {
    Utilities.showToast(context, "Authentication cancelled by user");
    Utilities.clearAllSpinners(context);
    clearMembers();
  }

  @Override
  public void onAuthenticationSuccessful() {
    Utilities.sendCommand(context, userID, outboundTriggerOrValue, true, callType);
    clearMembers();
  }

  @Override
  public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
    Utilities.showToast(context, helpString.toString());
    Utilities.clearAllSpinners(context);
    clearMembers();
  }

  @Override
  public void onAuthenticationError(int errorCode, CharSequence errString) {
    Utilities.showToast(context, errString.toString());
    Utilities.clearAllSpinners(context);
    clearMembers();
  }
}
