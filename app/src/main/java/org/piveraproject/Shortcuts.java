package org.piveraproject;

import java.util.ArrayList;

import org.piveraproject.utilities.Utilities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

public class Shortcuts extends AppCompatActivity {

  private final String IS_SECURE_KEY = "isSecure";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Bundle extras = getIntent().getExtras();

    if (extras != null && extras.size() > 0) {
      String body = "";
      ArrayList payloadList = new ArrayList();
      boolean isSecure = false;

      for (String key : extras.keySet()) {
        Object value = extras.get(key);
        if (key.equalsIgnoreCase(IS_SECURE_KEY)) {
          isSecure = true;
        } else {
          payloadList.add(key.toString());
          payloadList.add(value.toString());
        }
      }

      // Turn arraylist into 'separator'-separated list.
      body = android.text.TextUtils.join("|", payloadList);

      // If it's secure, open the main app so we can authenticate there.
      if (isSecure) {
        Intent intent = new Intent(this, Main.class);
        Bundle b = new Bundle();
        b.putString(PiVeraProjectConstants.CROSS_ACTIVITY_PAYLOAD_KEY, body);
        intent.putExtras(b);
        startActivity(intent);
      } else {
        Utilities.prepareSend(
            this.getApplicationContext(),
            body,
            "",
            isSecure,
            PiVeraProjectConstants.APICallType.POST);
      }
    } else {
      Utilities.postNotification(
          this.getApplicationContext(), "Malformed Extras, expecting at least one 'key:value'");
    }
    this.finish();
  }
}
