package org.piveraproject;

import androidx.appcompat.app.AppCompatActivity;

public class PiVeraProjectConstants extends AppCompatActivity {
  public static final int UNIQUE_ID_LENGTH = 8;
  public static final String PREFNAME_USERID = "userID";
  public static final String PREFNAME_USERPASS = "userPass";
  public static final String CROSS_ACTIVITY_PAYLOAD_KEY = "CROSS_ACTIVITY_PAYLOAD_KEY";

  public enum ElementType {
    INTONOFF,
    ONOFF,
    JUSTSEND;

    public static ElementType getType(String input) {
      for (ElementType type : ElementType.values()) {
        if (input.equalsIgnoreCase(type.name())) {
          return type;
        }
      }
      return JUSTSEND;
    }
  }

  public enum APICallType {
    POST("POST"),
    GET_ALL("GET"),
    GET_ONE("GET");

    public String requestMethodName;

    APICallType(String requestMethodName) {
      this.requestMethodName = requestMethodName;
    }
  }

  public static final String PREFNAME_DESTINATION_ADDRESS = "pref_destination_address";
  public static final String PREFNAME_FILENAME = "pref_destination_filename";
  public static final String PREFNAME_VERSION = "pref_version";

  public static final String DEFAULT_PI_ADDRESS = "https://YourDynamicDNS.duckdns.org";
  public static final String DEFAULT_VERSION = "FIRSTRUN";
  public static final String DEFAULT_FILE_NAME = "AutomationLog.txt";

  public static final String GET_ALL_RESOURCE = "/devices";
  public static final String GET_ONE_RESOURCE = "/device";
  public static final String POST_ONE_RESOURCE = "/device";

  public static final int NOTIFICATION_ID = 101;
  public static final String NOTIFICATION_TITLE = "Server Error";

  public static final String SEND_SEPARATOR = "|";
}
