package org.piveraproject.auth;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.biometric.BiometricManager;
import androidx.core.app.ActivityCompat;

@SuppressLint({"MissingPermission"})
public class BiometricUtils {

  public static boolean isBiometricPromptEnabled() {
    return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P);
  }

  /*
   * Condition: Check if the device has fingerprint sensors.
   * Note: If you marked android.hardware.fingerprint as something that
   * your app requires (android:required="true"), then you don't need
   * to perform this check.
   *
   * */
  public static boolean isBiometricsSupported(Context context) {
    BiometricManager biometricManager = BiometricManager.from(context);
    if (biometricManager.canAuthenticate() == BiometricManager.BIOMETRIC_SUCCESS) {
      return true;
    }
    return false;
  }

  /*
   * Condition: Check if the permission has been added to
   * the app. This permission will be granted as soon as the user
   * installs the app on their device.
   *
   * */
  public static boolean isPermissionGranted(Context context) {
    return ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT)
        == PackageManager.PERMISSION_GRANTED;
  }
}
