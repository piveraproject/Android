package org.piveraproject.display;

import org.piveraproject.PiVeraProjectConstants;
import org.piveraproject.R;
import org.piveraproject.utilities.Utilities;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class Settings extends AppCompatActivity {
  private TextView address;
  private TextView filename;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.settings_layout);

    address = (EditText) (findViewById(R.id.address));
    filename = (EditText) (findViewById(R.id.filename));

    ((TextView) (findViewById(R.id.version))).setText(Utilities.getVersionNumber(this));

    String previousSelectedAddress =
        Utilities.getPrefValue(
            this,
            PiVeraProjectConstants.PREFNAME_DESTINATION_ADDRESS,
            PiVeraProjectConstants.DEFAULT_PI_ADDRESS);
    if (previousSelectedAddress.isEmpty()) {
      address.setText(PiVeraProjectConstants.DEFAULT_PI_ADDRESS);
    } else {
      address.setText(previousSelectedAddress);
    }

    String previousSelectedFilename =
        Utilities.getPrefValue(
            this,
            PiVeraProjectConstants.PREFNAME_FILENAME,
            PiVeraProjectConstants.DEFAULT_FILE_NAME);
    filename.setText(previousSelectedFilename);
  }

  @Override
  public void onBackPressed() {
    Utilities.storePrefValue(
        this,
        PiVeraProjectConstants.PREFNAME_DESTINATION_ADDRESS,
        address.getText().toString().trim());
    Utilities.storePrefValue(
        this, PiVeraProjectConstants.PREFNAME_FILENAME, filename.getText().toString().trim());
    super.onBackPressed();
  }
}
