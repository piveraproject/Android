package org.piveraproject.display;

public class InstructionsButton implements GenericButton {
  public String instructions = "Configure settings, then pull to refresh Command List...";

  public String getInstructions() {
    return instructions;
  }
}
