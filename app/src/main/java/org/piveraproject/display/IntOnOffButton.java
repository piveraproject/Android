package org.piveraproject.display;

public class IntOnOffButton implements GenericButton, Updatable {
  private String displayedCommand;
  private String actualCommand;
  private String button1Text;
  private String button2Text;
  private String button1Command;
  private String button2Command;
  private boolean secure = false;
  private String currentValue;
  private boolean confirm = false;

  public boolean shouldConfirm() {
    return confirm;
  }

  public IntOnOffButton setConfirm(boolean confirm) {
    this.confirm = confirm;
    return this;
  }

  public String getDisplayedCommand() {
    return displayedCommand;
  }

  public String getActualCommand() {
    return actualCommand;
  }

  public String getButton1Text() {
    return button1Text;
  }

  public String getButton2Text() {
    return button2Text;
  }

  public boolean isSecure() {
    return secure;
  }

  public String getCurrentValue() {
    return currentValue;
  }

  public String getButton1Command() {
    return button1Command;
  }

  public String getButton2Command() {
    return button2Command;
  }

  public IntOnOffButton setCurrentValue(String currentValue) {
    this.currentValue = currentValue;
    return this;
  }

  public IntOnOffButton setSecure(boolean secure) {
    this.secure = secure;
    return this;
  }

  public IntOnOffButton setDisplayedCommand(String displayedCommand) {
    this.displayedCommand = displayedCommand;
    return this;
  }

  public IntOnOffButton setActualCommand(String actualCommand) {
    this.actualCommand = actualCommand;
    return this;
  }

  public IntOnOffButton setButton1Text(String button1Text) {
    this.button1Text = button1Text;
    return this;
  }

  public IntOnOffButton setButton2Text(String button2Text) {
    this.button2Text = button2Text;
    return this;
  }

  public IntOnOffButton setButton1Command(String text) {
    this.button1Command = text;
    return this;
  }

  public IntOnOffButton setButton2Command(String text) {
    this.button2Command = text;
    return this;
  }

  @Override
  public void callVeraToGetValue() {}
}
