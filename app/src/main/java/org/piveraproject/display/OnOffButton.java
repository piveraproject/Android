package org.piveraproject.display;

public class OnOffButton implements GenericButton, Updatable {
  private String displayedCommand;
  private String actualCommand;
  private String onButtonText;
  private String onButtonTrigger;
  private String offButtonText;
  private String offButtonTrigger;
  private boolean secure = false;
  private String onButtonValue;
  private String offButtonValue;
  //  private String currentValue;
  private boolean confirm = false;

  public String getOnButtonValue() {
    return onButtonValue;
  }

  public OnOffButton setOnButtonValue(String onButtonValue) {
    this.onButtonValue = onButtonValue;
    return this;
  }

  public String getOffButtonValue() {
    return offButtonValue;
  }

  public OnOffButton setOffButtonValue(String offButtonValue) {
    this.offButtonValue = offButtonValue;
    return this;
  }

  public boolean shouldConfirm() {
    return confirm;
  }

  public OnOffButton setConfirm(boolean confirm) {
    this.confirm = confirm;
    return this;
  }

  public String getOnButtonTrigger() {
    return onButtonTrigger;
  }

  public OnOffButton setOnButtonTrigger(String onButtonTrigger) {
    this.onButtonTrigger = onButtonTrigger;
    return this;
  }

  public String getOffButtonTrigger() {
    return offButtonTrigger;
  }

  public OnOffButton setOffButtonTrigger(String offButtonTrigger) {
    this.offButtonTrigger = offButtonTrigger;
    return this;
  }

  public String getDisplayedCommand() {
    return displayedCommand;
  }

  public String getActualCommand() {
    return actualCommand;
  }

  public String getOnButtonText() {
    return onButtonText;
  }

  public String getOffButtonText() {
    return offButtonText;
  }

  public boolean isSecure() {
    return secure;
  }

  //  public String getCurrentValue() {
  //    return currentValue;
  //  }
  //
  //  public OnOffButton setCurrentValue(String currentValue) {
  //    this.currentValue = currentValue;
  //    return this;
  //  }

  public OnOffButton setSecure(boolean secure) {
    this.secure = secure;
    return this;
  }

  public OnOffButton setDisplayedCommand(String displayedCommand) {
    this.displayedCommand = displayedCommand;
    return this;
  }

  public OnOffButton setActualCommand(String actualCommand) {
    this.actualCommand = actualCommand;
    return this;
  }

  public OnOffButton setOnButtonText(String onButtonText) {
    this.onButtonText = onButtonText;
    return this;
  }

  public OnOffButton setOffButtonText(String offButtonText) {
    this.offButtonText = offButtonText;
    return this;
  }

  @Override
  public void callVeraToGetValue() {}
}
