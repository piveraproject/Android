package org.piveraproject.display;

public interface Updatable {
  public void callVeraToGetValue();
}
