package org.piveraproject.display;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.piveraproject.PiVeraProjectConstants;
import org.piveraproject.R;
import org.piveraproject.serveritemtypes.GenericItem;
import org.piveraproject.serveritemtypes.IntegerItem;
import org.piveraproject.serveritemtypes.LockItem;
import org.piveraproject.serveritemtypes.OnOffItem;
import org.piveraproject.serveritemtypes.SceneOnOffItem;
import org.piveraproject.serveritemtypes.SceneOnOffReadDeviceStateItem;
import org.piveraproject.serveritemtypes.SceneOnlyOnItem;
import org.piveraproject.utilities.Utilities;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

import static org.piveraproject.PiVeraProjectConstants.APICallType.GET_ONE;
import static org.piveraproject.PiVeraProjectConstants.APICallType.POST;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.PrimaryListHolder> {
  private ArrayList<GenericButton> dataSet = new ArrayList();
  private static int errorCount;
  private Context context;

  public class PrimaryListHolder extends RecyclerView.ViewHolder {

    public PiVeraProjectConstants.ElementType type;
    public String friendlyName;
    public String command;
    public String buttonOnText;
    public String buttonOnOutboundTrigger;
    public String buttonOffText;
    public String buttonOffOutboundTrigger;
    public TextView label;
    public TextView newValue;
    public Button offButton;
    public Button onButton;
    public Button intOnOffSend;
    public Button justSendButton;
    public boolean isSecure = false;
    public boolean shouldConfirm = false;
    public ProgressBar progressBar;

    public PrimaryListHolder(View itemView) {
      super(itemView);
      label = itemView.findViewById(R.id.deviceName);
      newValue = (EditText) itemView.findViewById(R.id.newValue);
      onButton = itemView.findViewById(R.id.buttonRight);
      offButton = itemView.findViewById(R.id.buttonLeft);
      intOnOffSend = itemView.findViewById(R.id.SendButton);
      justSendButton = itemView.findViewById(R.id.justSendButton);
      progressBar = itemView.findViewById(R.id.progressBar);
    }
  }

  public ListAdapter(Context context, GenericItem[] dataset) {
    this.context = context;
    buildDataSet(dataset);
  }

  public void updateSingleDevice(GenericItem item) {
    for (GenericButton element : this.dataSet) {
      // Order matters here, because of Inheritance and instanceOf
      if (element instanceof IntOnOffButton) {
        if (((IntOnOffButton) element)
            .getActualCommand()
            .equalsIgnoreCase(item.getName().getKey())) {
          ((IntOnOffButton) element).setCurrentValue(item.getCurrentValue());
          notifyDataSetChanged();
          break;
        }
      } else if (element instanceof OnOffButton) {
        if (((OnOffButton) element).getActualCommand().equalsIgnoreCase(item.getName().getKey())) {
          //          String currentValue = item.getStateBasedOnTrigger("1").getCurrentValue();
          //          if (currentValue != null) {
          //            ((OnOffButton) element).setCurrentValue("1".equalsIgnoreCase(currentValue) ?
          // "1" : "0");
          //            notifyDataSetChanged();
          //            break;
          //          }
          ((OnOffButton) element)
              .setOnButtonValue(item.getStateBasedOnTrigger("1").getCurrentValue());
          ((OnOffButton) element)
              .setOffButtonValue(item.getStateBasedOnTrigger("0").getCurrentValue());
          break;
        }
      }
    }
  }

  public void refreshEveryRowsValue() {
    for (GenericButton button : dataSet) {
      if (button instanceof OnOffButton) {
        Utilities.prepareSend(
            context, ((OnOffButton) button).getActualCommand(), "99", false, GET_ONE);
      } else if (button instanceof IntOnOffButton) {
        Utilities.prepareSend(
            context, ((IntOnOffButton) button).getActualCommand(), "99", false, GET_ONE);
      }
    }
  }

  public void buildDataSet(GenericItem[] dataset) {
    for (GenericItem item : dataset) {
      try {
        if (item.getItemType().equalsIgnoreCase(OnOffItem.class.getSimpleName())
            || item.getItemType()
                .equalsIgnoreCase(SceneOnOffReadDeviceStateItem.class.getSimpleName())
            || item.getItemType().equalsIgnoreCase(LockItem.class.getSimpleName())) {
          this.dataSet.add(
              new OnOffButton()
                  .setDisplayedCommand(item.getName().getHumanReadable())
                  .setActualCommand(item.getName().getKey())
                  .setOnButtonText(item.getStateBasedOnTrigger("1").getButtonText())
                  .setOnButtonTrigger(item.getStateBasedOnTrigger("1").getTrigger())
                  .setOffButtonText(item.getStateBasedOnTrigger("0").getButtonText())
                  .setOffButtonTrigger(item.getStateBasedOnTrigger("0").getTrigger())
                  .setOnButtonValue(item.getStateBasedOnTrigger("1").getCurrentValue())
                  .setOffButtonValue(item.getStateBasedOnTrigger("0").getCurrentValue())
                  .setSecure(item.isSecure())
                  .setConfirm(item.isRequireConfirmation()));
        } else if (item.getItemType().equalsIgnoreCase(SceneOnOffItem.class.getSimpleName())) {
          this.dataSet.add(
              new OnOffButton()
                  .setDisplayedCommand(item.getName().getHumanReadable())
                  .setActualCommand(item.getName().getKey())
                  .setOnButtonText(item.getStateBasedOnTrigger("1").getButtonText())
                  .setOnButtonTrigger(item.getStateBasedOnTrigger("1").getTrigger())
                  .setOffButtonText(item.getStateBasedOnTrigger("0").getButtonText())
                  .setOffButtonTrigger(item.getStateBasedOnTrigger("0").getTrigger())
                  .setSecure(item.isSecure())
                  .setConfirm(item.isRequireConfirmation()));
        } else if (item.getItemType().equalsIgnoreCase(IntegerItem.class.getSimpleName())) {
          this.dataSet.add(
              new IntOnOffButton()
                  .setDisplayedCommand(item.getName().getHumanReadable())
                  .setActualCommand(item.getName().getKey())
                  .setSecure(item.isSecure())
                  .setCurrentValue(item.getCurrentValue())
                  .setConfirm(item.isRequireConfirmation()));
        } else if (item.getItemType().equalsIgnoreCase(SceneOnlyOnItem.class.getSimpleName())) {
          this.dataSet.add(
              new JustSendButton()
                  .setDisplayedCommand(item.getName().getHumanReadable())
                  .setActualCommand(item.getName().getKey())
                  .setButtonText(item.getButtonText())
                  .setSecure(item.isSecure())
                  .setConfirm(item.isRequireConfirmation()));
        }
      } catch (Exception e) {
        // ignore it, continue building the list.
      }
    }
  }

  public ArrayList getDataSet() {
    return dataSet;
  }

  // Create new views (invoked by the layout manager)
  @Override
  public PrimaryListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    return new PrimaryListHolder(
        LayoutInflater.from(parent.getContext()).inflate(R.layout.listrecycler, parent, false));
  }

  public void handleSendNewValueEvent(
      final Context context,
      final String command,
      final String outboundTriggerOrValue,
      final boolean isSecure,
      final PiVeraProjectConstants.APICallType callType,
      String humanReadable,
      String newValue,
      boolean isRequireConfirmation,
      final PrimaryListHolder holder) {

    if (isRequireConfirmation) {
      new AlertDialog.Builder(context)
          .setMessage("Confirm:  " + humanReadable + "  -  " + newValue + "?")
          .setPositiveButton(
              "CONFIRM",
              new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                  holder.progressBar.setVisibility(View.VISIBLE);
                  Utilities.prepareSend(context, command, outboundTriggerOrValue, isSecure, POST);
                }
              })
          .setNegativeButton(
              "CANCEL",
              new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                  // just let the dialog cancel.
                }
              })
          .create()
          .show();
    } else {
      holder.progressBar.setVisibility(View.VISIBLE);
      Utilities.prepareSend(context, command, outboundTriggerOrValue, isSecure, POST);
    }
  }

  // Replace the contents of a view (invoked by the layout manager)
  @Override
  public void onBindViewHolder(final PrimaryListHolder holder, final int position) {

    GenericButton button = dataSet.get(position);

    holder.label.setVisibility(View.VISIBLE);

    holder.intOnOffSend.setVisibility(View.INVISIBLE);
    holder.newValue.setVisibility(View.INVISIBLE);
    holder.justSendButton.setVisibility(View.INVISIBLE);
    holder.offButton.setVisibility(View.INVISIBLE);
    holder.onButton.setVisibility(View.INVISIBLE);
    holder.progressBar.setVisibility(View.INVISIBLE);

    try {
      if (button instanceof InstructionsButton) {
        // this is an 'initial setup' condition.

        holder.label.setVisibility(View.VISIBLE);
        holder.label.setText(((InstructionsButton) button).getInstructions());

      } else if (button instanceof OnOffButton) {
        holder.type = PiVeraProjectConstants.ElementType.ONOFF;
        holder.command = ((OnOffButton) button).getActualCommand();
        holder.friendlyName = ((OnOffButton) button).getDisplayedCommand();
        holder.buttonOnText = ((OnOffButton) button).getOnButtonText();
        holder.buttonOnOutboundTrigger = ((OnOffButton) button).getOnButtonTrigger();
        holder.buttonOffText = ((OnOffButton) button).getOffButtonText();
        holder.buttonOffOutboundTrigger = ((OnOffButton) button).getOffButtonTrigger();
        holder.isSecure = ((OnOffButton) button).isSecure();
        holder.label.setText(holder.friendlyName);
        holder.offButton.setText(holder.buttonOffText);
        holder.onButton.setText(holder.buttonOnText);
        holder.shouldConfirm = ((OnOffButton) button).shouldConfirm();

        holder.offButton.setVisibility(View.VISIBLE);
        holder.onButton.setVisibility(View.VISIBLE);

        /*In the case where permission is removed (server-side), and the screen is refreshed,
        items will find themselves in different locations. This can cause an issue with button
        background information. For good measure, clear the background of both buttons.*/
        holder.offButton.setBackground(context.getResources().getDrawable(R.drawable.button_plain));
        holder.onButton.setBackground(context.getResources().getDrawable(R.drawable.button_plain));

        if ("1".equalsIgnoreCase(((OnOffButton) button).getOnButtonValue())) {
          holder.onButton.setBackground(
              context.getResources().getDrawable(R.drawable.button_with_border));
        }
        if ("1".equalsIgnoreCase(((OnOffButton) button).getOffButtonValue())) {
          holder.offButton.setBackground(
              context.getResources().getDrawable(R.drawable.button_with_border));
        }

        //        String currentValue = ((OnOffButton) button).getCurrentValue();
        //        if (StringUtils.isNotEmpty(currentValue) && "1".equalsIgnoreCase(currentValue)) {
        //          holder.onButton.setBackground(
        //                  context.getResources().getDrawable(R.drawable.button_with_border));
        //        } else if (StringUtils.isNotEmpty(currentValue) &&
        // "0".equalsIgnoreCase(currentValue)) {
        //          holder.offButton.setBackground(
        //                  context.getResources().getDrawable(R.drawable.button_with_border));
        //        }

        holder.offButton.setOnClickListener(
            new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                handleSendNewValueEvent(
                    context,
                    holder.command,
                    holder.buttonOffOutboundTrigger,
                    holder.isSecure,
                    POST,
                    holder.friendlyName,
                    holder.buttonOffText,
                    holder.shouldConfirm,
                    holder);
              }
            });
        holder.onButton.setOnClickListener(
            new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                handleSendNewValueEvent(
                    context,
                    holder.command,
                    holder.buttonOnOutboundTrigger,
                    holder.isSecure,
                    POST,
                    holder.friendlyName,
                    holder.buttonOnText,
                    holder.shouldConfirm,
                    holder);
              }
            });

      } else if (button instanceof JustSendButton) {

        holder.justSendButton.setVisibility(View.VISIBLE);

        holder.type = PiVeraProjectConstants.ElementType.JUSTSEND;
        holder.friendlyName = ((JustSendButton) button).getDisplayedCommand();
        holder.command = ((JustSendButton) button).getActualCommand();
        holder.buttonOnText = ((JustSendButton) button).getButtonText();
        holder.isSecure = ((JustSendButton) button).isSecure();
        holder.label.setText(holder.friendlyName);
        holder.justSendButton.setText(holder.buttonOnText);
        holder.shouldConfirm = ((JustSendButton) button).shouldConfirm();

        holder.justSendButton.setOnClickListener(
            new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                handleSendNewValueEvent(
                    context,
                    holder.command,
                    "",
                    holder.isSecure,
                    POST,
                    holder.friendlyName,
                    holder.buttonOnText,
                    holder.shouldConfirm,
                    holder);
              }
            });

      } else {
        holder.intOnOffSend.setVisibility(View.VISIBLE);
        holder.newValue.setVisibility(View.VISIBLE);
        holder.offButton.setVisibility(View.VISIBLE);
        holder.onButton.setVisibility(View.VISIBLE);

        holder.type = PiVeraProjectConstants.ElementType.INTONOFF;
        holder.friendlyName = ((IntOnOffButton) button).getDisplayedCommand();
        holder.command = ((IntOnOffButton) button).getActualCommand();
        holder.buttonOnText = "100";
        holder.buttonOnOutboundTrigger = "100";
        holder.buttonOffText = "Off";
        holder.buttonOffOutboundTrigger = "0";
        holder.isSecure = ((IntOnOffButton) button).isSecure();
        holder.shouldConfirm = ((IntOnOffButton) button).shouldConfirm();
        holder.label.setText(holder.friendlyName);
        holder.offButton.setText(holder.buttonOffText);
        holder.onButton.setText(holder.buttonOnText);

        /*In the case where permission is removed (server-side), and the screen is refreshed,
        items will find themselves in different locations. This can cause an issue with button
        background information. For good measure, clear the background of both buttons.*/
        holder.offButton.setBackground(context.getResources().getDrawable(R.drawable.button_plain));
        holder.onButton.setBackground(context.getResources().getDrawable(R.drawable.button_plain));

        String currentValue = ((IntOnOffButton) button).getCurrentValue();
        holder.newValue.setText(currentValue);
        if ("0".equalsIgnoreCase(currentValue)) {
          holder.offButton.setBackground(
              context.getResources().getDrawable(R.drawable.button_with_border));
          holder.newValue.setText(currentValue);
        } else if ("100".equalsIgnoreCase(currentValue)) {
          holder.onButton.setBackground(
              context.getResources().getDrawable(R.drawable.button_with_border));
        }

        holder.offButton.setOnClickListener(
            new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                handleSendNewValueEvent(
                    context,
                    holder.command,
                    holder.buttonOffOutboundTrigger,
                    holder.isSecure,
                    POST,
                    holder.friendlyName,
                    holder.buttonOffText,
                    holder.shouldConfirm,
                    holder);
              }
            });
        holder.onButton.setOnClickListener(
            new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                handleSendNewValueEvent(
                    context,
                    holder.command,
                    holder.buttonOnOutboundTrigger,
                    holder.isSecure,
                    POST,
                    holder.friendlyName,
                    holder.buttonOnText,
                    holder.shouldConfirm,
                    holder);
              }
            });
        holder.intOnOffSend.setOnClickListener(
            new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                handleSendNewValueEvent(
                    context,
                    holder.command,
                    holder.newValue.getText().toString().trim(),
                    holder.isSecure,
                    POST,
                    holder.friendlyName,
                    holder.newValue.getText().toString().trim(),
                    holder.shouldConfirm,
                    holder);
              }
            });
      }
    } catch (Exception e) {
      errorCount = errorCount + 1;
    }
  }

  public static int getErrorCount() {
    return errorCount;
  }

  public static int setErrorCount(int newVal) {
    return errorCount = newVal;
  }

  // Return the size of your dataSet (invoked by the layout manager).
  @Override
  public int getItemCount() {
    return dataSet.size();
  }
}
