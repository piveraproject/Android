package org.piveraproject.display;

public class JustSendButton implements GenericButton {
  private String displayedCommand;
  private String actualCommand;
  private String buttonText;
  private boolean secure = false;
  private boolean confirm = false;

  public boolean shouldConfirm() {
    return confirm;
  }

  public JustSendButton setConfirm(boolean confirm) {
    this.confirm = confirm;
    return this;
  }

  public String getDisplayedCommand() {
    return displayedCommand;
  }

  public String getActualCommand() {
    return actualCommand;
  }

  public String getButtonText() {
    return buttonText;
  }

  public boolean isSecure() {
    return secure;
  }

  public JustSendButton setButtonText(String buttonText) {
    this.buttonText = buttonText;
    return this;
  }

  public JustSendButton setSecure(boolean secure) {
    this.secure = secure;
    return this;
  }

  public JustSendButton setDisplayedCommand(String displayedCommand) {
    this.displayedCommand = displayedCommand;
    return this;
  }

  public JustSendButton setActualCommand(String actualCommand) {
    this.actualCommand = actualCommand;
    return this;
  }
}
