package org.piveraproject.utilities;

import org.piveraproject.data_transfer.SendDelegate;
import org.piveraproject.display.Settings;

import android.content.Context;
import android.content.Intent;

public class OpenSettingsDelegate extends SendDelegate {
  private Context context;

  public OpenSettingsDelegate(Context context) {
    super(context);
    this.context = context;
  }

  @Override
  public void onAuthenticationSuccessful() {
    context.startActivity(new Intent(context, Settings.class));
    clearMembers();
  }
}
