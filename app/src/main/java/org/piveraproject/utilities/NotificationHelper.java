package org.piveraproject.utilities;

import org.piveraproject.Main;
import org.piveraproject.R;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Build;

class NotificationHelper extends ContextWrapper {
  private NotificationManager notifManager;
  public static final String CHANNEL_ONE_ID = "PiVeraProjectChannel";
  public static final String CHANNEL_ONE_NAME = "ChannelOne";

  Context context;

  public NotificationHelper(Context base) {
    super(base);
    context = base;
    createChannels();
  }

  @TargetApi(Build.VERSION_CODES.O)
  private void createChannels() {
    NotificationChannel notificationChannel =
        new NotificationChannel(CHANNEL_ONE_ID, CHANNEL_ONE_NAME, notifManager.IMPORTANCE_HIGH);
    notificationChannel.setShowBadge(true);
    notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
    getManager().createNotificationChannel(notificationChannel);
  }

  @TargetApi(Build.VERSION_CODES.O)
  public Notification.Builder getNotification(String title, String body) {
    return new Notification.Builder(context, CHANNEL_ONE_ID)
        .setContentTitle(title)
        .setContentText(body)
        .setStyle(new Notification.BigTextStyle().bigText(body))
        .setSmallIcon(R.mipmap.icon_small)
        .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(context, Main.class), 0))
        .setAutoCancel(true);
  }

  public void notify(int id, Notification.Builder notification) {
    getManager().notify(id, notification.build());
  }

  private NotificationManager getManager() {
    if (notifManager == null) {
      notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }
    return notifManager;
  }
}
