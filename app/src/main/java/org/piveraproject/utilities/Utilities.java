package org.piveraproject.utilities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.piveraproject.Main;
import org.piveraproject.PiVeraProjectConstants;
import org.piveraproject.R;
import org.piveraproject.auth.PiVeraBiometricManager;
import org.piveraproject.data_transfer.PiVeraAPI;
import org.piveraproject.data_transfer.SendDelegate;

import android.app.Notification;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class Utilities extends AppCompatActivity {

  public static boolean isFirstRun(Context context) {
    if (Utilities.getPrefValue(
            context,
            PiVeraProjectConstants.PREFNAME_VERSION,
            PiVeraProjectConstants.DEFAULT_VERSION)
        .trim()
        .equalsIgnoreCase(PiVeraProjectConstants.DEFAULT_VERSION.trim())) {
      return true;
    }
    return false;
  }

  public static void clearAllSpinners(final Context context) {
    if (context instanceof Main) {
      ((Main) context)
          .runOnUiThread(
              new Runnable() {
                @Override
                public void run() {
                  ((Main) context).getInstance().stopRefreshingAnimation();
                  ((Main) context).getInstance().refreshDataSet();
                }
              });
    }
  }

  public static void postNotification(Context context, String body) {
    NotificationHelper notificationHelper = new NotificationHelper(context);
    Notification.Builder notificationBuilder =
        notificationHelper.getNotification(PiVeraProjectConstants.NOTIFICATION_TITLE, body);
    notificationHelper.notify(PiVeraProjectConstants.NOTIFICATION_ID, notificationBuilder);
    appendStringToFile(context, "[Notification] " + body);
  }

  public static String getVersionNumber(Context context) {
    String version = "";
    try {
      version = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
    } catch (Exception e) {
    }
    return version;
  }

  public static void appendStringToFile(Context context, String msg) {
    try {
      Calendar c = Calendar.getInstance();
      SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      String formattedDate = df.format(c.getTime());
      msg = formattedDate + " " + msg;

      String filename =
          Utilities.getPrefValue(
                  context,
                  PiVeraProjectConstants.PREFNAME_FILENAME,
                  PiVeraProjectConstants.DEFAULT_FILE_NAME)
              .trim();
      File file =
          Environment.getExternalStoragePublicDirectory(
              Environment.DIRECTORY_DOCUMENTS + File.separator + filename);
      file.getParentFile().mkdirs();

      file.createNewFile(); // ok if returns false, overwrite
      Writer out = new BufferedWriter(new FileWriter(file, true), 1024);
      out.write(msg + "\n");
      out.close();

    } catch (Exception e) {
    }
  }

  public static boolean isDataConnectionActive(Context context) {
    NetworkInfo activeNetwork =
        ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE))
            .getActiveNetworkInfo();
    if (activeNetwork != null && activeNetwork.isConnected()) {
      return true;
    }
    return false;
  }

  public static void prepareSend(
      Context context,
      String command,
      String outboundTriggerOrValue,
      boolean isSecure,
      PiVeraProjectConstants.APICallType callType) {
    if (isDataConnectionActive(context)) {
      sendViaDataConnection(context, command, outboundTriggerOrValue, isSecure, callType);
    } else {
      postNotification(context, "Failed to send command: do you have a data connection?");
    }
  }

  private static void sendViaDataConnection(
      Context context,
      String command,
      String outboundTriggerOrValue,
      boolean isSecure,
      PiVeraProjectConstants.APICallType callType) {
    if (isSecure) {
      new PiVeraBiometricManager.BiometricBuilder(context)
          .setTitle(context.getString(R.string.biometric_title))
          .setNegativeButtonText(context.getString(R.string.biometric_negative_button_text))
          .build()
          .authenticate(new SendDelegate(context, command, outboundTriggerOrValue, callType));
    } else {
      sendCommand(context, command, outboundTriggerOrValue, false, callType);
    }
  }

  public static void sendCommand(
      Context context,
      String deviceName,
      String outboundTriggerOrValue,
      boolean isSecure,
      PiVeraProjectConstants.APICallType callType) {
    String userID = Utilities.getPrefValue(context, PiVeraProjectConstants.PREFNAME_USERID, "");
    String baseAddress =
        Utilities.getPrefValue(
                context,
                PiVeraProjectConstants.PREFNAME_DESTINATION_ADDRESS,
                PiVeraProjectConstants.DEFAULT_PI_ADDRESS)
            .trim();

    if (callType == PiVeraProjectConstants.APICallType.GET_ALL) {
      new PiVeraAPI().performGetAll(context, baseAddress, userID);
    } else if (callType == PiVeraProjectConstants.APICallType.GET_ONE) {
      new PiVeraAPI().performGetOne(context, baseAddress, userID, deviceName);
    } else {
      new PiVeraAPI().performPost(context, baseAddress, userID, deviceName, outboundTriggerOrValue);
      showToast(context, "Command Sent.");
      appendStringToFile(context, deviceName + " " + outboundTriggerOrValue);
    }
  }

  public static void showToast(Context context, String text) {
    Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
  }

  private static SharedPreferences getPreference(Context context) {
    return PreferenceManager.getDefaultSharedPreferences(context);
  }

  public static String getPrefValue(Context context, String prefName, String defaultValue) {
    return decrypt(getPreference(context).getString(encrypt(prefName), encrypt(defaultValue)));
  }

  public static void storePrefValue(Context context, String prefName, String value) {
    getPreference(context)
        .edit()
        .putString(Utilities.encrypt(prefName), Utilities.encrypt(value))
        .commit();
  }

  private static String encrypt(String input) {
    // This is base64 encoding, which is not an encryption, but obfuscates the value enough for now.
    return Base64.encodeToString(input.getBytes(), Base64.DEFAULT);
  }

  private static String decrypt(String input) {
    return new String(Base64.decode(input, Base64.DEFAULT));
  }

  public static String getUserPasscode(Context context) {
    return Utilities.getPrefValue(context, PiVeraProjectConstants.PREFNAME_USERPASS, "");
  }
}
