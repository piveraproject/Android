package org.piveraproject.serveritemtypes;

import java.util.ArrayList;
import java.util.function.Predicate;

public class GenericItem implements Comparable<GenericItem> {

  private Name name;
  private boolean secure = false;
  private boolean requireConfirmation = false;
  public String currentValue;
  private ArrayList<SelectableState> eligibleStates = new ArrayList();
  private String itemType;
  private String buttonText;

  public String getButtonText() {
    return buttonText;
  }

  public void setButtonText(String buttonText) {
    this.buttonText = buttonText;
  }

  public Name getName() {
    return name;
  }

  public void setName(Name name) {
    this.name = name;
  }

  public boolean isSecure() {
    return secure;
  }

  public void setSecure(boolean secure) {
    this.secure = secure;
  }

  public boolean isRequireConfirmation() {
    return requireConfirmation;
  }

  public void setRequireConfirmation(boolean requireConfirmation) {
    this.requireConfirmation = requireConfirmation;
  }

  public String getCurrentValue() {
    return currentValue;
  }

  public void setCurrentValue(String currentValue) {
    this.currentValue = currentValue;
  }

  public ArrayList<SelectableState> getEligibleStates() {
    return eligibleStates;
  }

  public void setEligibleStates(ArrayList<SelectableState> eligibleStates) {
    this.eligibleStates = eligibleStates;
  }

  public String getItemType() {
    return itemType;
  }

  public void setItemType(String itemType) {
    this.itemType = itemType;
  }

  public SelectableState getStateBasedOnTrigger(final String trigger) {
    return eligibleStates.stream()
        .filter(
            new Predicate<SelectableState>() {
              @Override
              public boolean test(SelectableState e) {
                return e.getTrigger().equalsIgnoreCase(trigger);
              }
            })
        .findAny()
        .orElse(null);
  }

  @Override
  public int compareTo(GenericItem genericItem) {
    return this.getName().getHumanReadable().compareTo(genericItem.getName().getHumanReadable());
  }
}
