package org.piveraproject.serveritemtypes;

import java.util.ArrayList;

public class OnOffItem extends GenericItem {

  public ArrayList<SelectableState> getEligibleStates() {
    return eligibleStates;
  }

  public void setEligibleStates(ArrayList<SelectableState> eligibleStates) {
    this.eligibleStates = eligibleStates;
  }

  private ArrayList<SelectableState> eligibleStates = new ArrayList();
}
