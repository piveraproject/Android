package org.piveraproject.serveritemtypes;

public class SceneOnlyOnItem extends GenericItem {
  public String getItemType() {
    return itemType;
  }

  public int getAddress() {
    return address;
  }

  public void setAddress(int address) {
    this.address = address;
  }

  public final String itemType = "SceneOnlyOnItem";
  private int address;
}
