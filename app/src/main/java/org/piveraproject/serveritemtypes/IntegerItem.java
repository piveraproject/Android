package org.piveraproject.serveritemtypes;

public class IntegerItem extends GenericItem {
  public int getAddress() {
    return address;
  }

  public void setAddress(int address) {
    this.address = address;
  }

  public String getCurrentValue() {
    return currentValue;
  }

  public void setCurrentValue(String currentValue) {
    this.currentValue = currentValue;
  }

  private int address;
  public String currentValue;
}
