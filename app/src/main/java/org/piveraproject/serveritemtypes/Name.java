package org.piveraproject.serveritemtypes;

public class Name {
  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getHumanReadable() {
    return humanReadable;
  }

  public void setHumanReadable(String humanReadable) {
    this.humanReadable = humanReadable;
  }

  private String key;
  private String humanReadable;
}
