package org.piveraproject;

import java.util.Random;

import org.piveraproject.auth.PiVeraBiometricManager;
import org.piveraproject.display.ListAdapter;
import org.piveraproject.serveritemtypes.GenericItem;
import org.piveraproject.utilities.OpenSettingsDelegate;
import org.piveraproject.utilities.Utilities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class Main extends AppCompatActivity {
  private RecyclerView listArea;
  private SwipeRefreshLayout swipeRefreshLayout;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);

    if (checkSelfPermission(Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
      ActivityCompat.requestPermissions(
          this,
          new String[] {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_NETWORK_STATE
          },
          1);
    }

    // Since biometrics arent considered a "dangerous" type by the OS, this is probably not
    // necessary, but I'm leaving it just in case.
    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_BIOMETRIC)
        != PackageManager.PERMISSION_GRANTED) {
      ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.USE_BIOMETRIC}, 1);
    }

    SwipeRefreshLayout.OnRefreshListener swipeRefreshListener =
        new SwipeRefreshLayout.OnRefreshListener() {
          @Override
          public void onRefresh() {
            /*Send an isSecure value of False here, because we don't want to prompt for fingerprint
            each time the menu is refreshed. The final call to get the list is hardcoded to include
            the user's passcode.*/
            Utilities.prepareSend(
                Main.this, "", "", false, PiVeraProjectConstants.APICallType.GET_ALL);
            ((ListAdapter) listArea.getAdapter()).getDataSet().clear();
            listArea.getAdapter().notifyDataSetChanged();
          }
        };

    swipeRefreshLayout.setOnRefreshListener(swipeRefreshListener);

    setAdapters(new GenericItem[0]);

    // Auto-refresh the app, but only if the user hasn't changed the default IP address.
    if (!Utilities.getPrefValue(
            this,
            PiVeraProjectConstants.PREFNAME_DESTINATION_ADDRESS,
            PiVeraProjectConstants.DEFAULT_PI_ADDRESS)
        .trim()
        .equalsIgnoreCase(PiVeraProjectConstants.DEFAULT_PI_ADDRESS.trim())) {
      swipeRefreshLayout.setRefreshing(true);
      swipeRefreshListener.onRefresh();
    }
  }

  @Override
  public void onNewIntent(Intent intent) {
    super.onNewIntent(intent);
    processTaskerShortcut(intent.getExtras());
  }

  private void processTaskerShortcut(Bundle bundle) {
    String payload = "";
    if (bundle != null) {
      payload = bundle.getString(PiVeraProjectConstants.CROSS_ACTIVITY_PAYLOAD_KEY, "");
      if (!payload.isEmpty()) {
        // It only would have been sent by Shortcuts class if it was a secure action.
        Utilities.prepareSend(this, payload, "", true, PiVeraProjectConstants.APICallType.POST);
      }
    }
  }

  public void stopRefreshingAnimation() {
    swipeRefreshLayout.setRefreshing(false);
  }

  @Override
  protected void onResume() {
    super.onResume();
    if (Utilities.isFirstRun(this)) {
      // Do first-run stuff here.
      showUniqueIDOnce(this);
    }
    Utilities.storePrefValue(
        Main.this, PiVeraProjectConstants.PREFNAME_VERSION, Utilities.getVersionNumber(this));
  }

  private void showUniqueIDOnce(Context context) {
    String userID = getRandomString();
    String userPass = getRandomString();
    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
    alertDialogBuilder
        .setTitle("Unique User Info")
        .setMessage(
            "This will only be showed once! \n\nYour unique user ID: "
                + userID
                + "\n\nYour personal passcode: "
                + userPass)
        .setCancelable(true)
        .setNegativeButton(
            "Done",
            new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
              }
            });
    AlertDialog alertDialog = alertDialogBuilder.create();
    alertDialog.show();
    Utilities.storePrefValue(Main.this, PiVeraProjectConstants.PREFNAME_USERID, userID);
    Utilities.storePrefValue(Main.this, PiVeraProjectConstants.PREFNAME_USERPASS, userPass);
  }

  private String getRandomString() {
    String[] options = {"1", "2", "3", "4", "5", "6", "7", "9"};
    String result = "";
    Random rand = new Random();
    for (int x = 0; x < PiVeraProjectConstants.UNIQUE_ID_LENGTH; x++) {
      result = result + options[rand.nextInt(options.length)];
    }
    return result;
  }

  public Main getInstance() {
    return this;
  }

  public void setAdapters(GenericItem[] data) {
    ListAdapter.setErrorCount(0);
    listArea = findViewById(R.id.listArea);
    listArea.setNestedScrollingEnabled(false);
    listArea.setAdapter(new ListAdapter(this, data));
    listArea.setLayoutManager(new LinearLayoutManager(this));
  }

  public void updateAllData(GenericItem[] list) {
    ((ListAdapter) listArea.getAdapter()).getDataSet().clear();
    ((ListAdapter) listArea.getAdapter()).buildDataSet(list);
    listArea.getAdapter().notifyDataSetChanged();
    int errors = ListAdapter.getErrorCount();
    if (errors > 0) {
      Utilities.postNotification(
          Main.this, "Error count in parsing response from server: " + errors);
    }
  }

  public void refreshEveryRowsValue() {
    //    ((ListAdapter) listArea.getAdapter()).refreshEveryRowsValue();
  }

  public void updateSingleLine(GenericItem genericItem) {
    ((ListAdapter) listArea.getAdapter()).updateSingleDevice(genericItem);
  }

  public void refreshDataSet() {
    listArea.getAdapter().notifyDataSetChanged();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.items, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    super.onOptionsItemSelected(item);
    switch (item.getItemId()) {
      case R.id.settings:
        new PiVeraBiometricManager.BiometricBuilder(Main.this)
            .setTitle(Main.this.getString(R.string.biometric_title))
            .setNegativeButtonText(Main.this.getString(R.string.biometric_negative_button_text))
            .build()
            .authenticate(new OpenSettingsDelegate(Main.this));

        break;
    }
    return true;
  }
}
